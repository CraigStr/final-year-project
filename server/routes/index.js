var express = require("express");
var router = express.Router();
require("dotenv").config();

const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_CONNECTION_STRING,
  MONGO_DB,
  JWKSURI,
  AUDIENCE,
  ISSUER,
} = process.env;

const mongoose = require("mongoose");
mongoose.connect(
  `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_CONNECTION_STRING}/${MONGO_DB}?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true }
);

const KDModel = mongoose.model("KD", {
  data: Object,
  date: Date,
  email: String,
});

var jwt = require("express-jwt");
var jwks = require("jwks-rsa");

var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: JWKSURI,
  }),
  audience: AUDIENCE,
  issuer: ISSUER,
  algorithms: ["RS256"],
});

router.post("/", jwtCheck, function (req, res, next) {
  if (!req.body.KD) res.status(422).send({ error: "Missing KD" });
  const KD = new KDModel({
    data: req.body.KD,
    date: Date.now(),
    email: req.body.email,
  });
  KD.save();

  res.status(201).json({message: "Submission Successful"});
});

module.exports = router;
