var express = require("express");
var texts = require("../texts/texts");
var router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  // let excerpt = texts[Math.floor(Math.random() * texts.length)];

  // const passageLength = 1000; // Roughly how many chatacters in excerpt? Rounded to nearest full sentence
  // let passageStart = Math.floor(
  //   Math.random() * (excerpt.length - passageLength)
  // );
  // let passageRange = [passageStart, passageStart + passageLength];

  // for (let i = passageRange[0]; i > 0; i--) {
  //   if (excerpt[i - 1] == ".") {
  //     // console.log(i)
  //     passageRange[0] = i;
  //     break;
  //   }
  // }
  // for (let i = passageRange[0]; i < passageRange[1]; i++) {
  //   if (/[a-zA-Z0-9]/.test(excerpt[i])) {
  //     // console.log(i)
  //     passageRange[0] = i;
  //     break;
  //   }
  // }

  // for (let i = passageRange[1]; i < excerpt.length; i++) {
  //   if (excerpt[i + 1] == ".") {
  //     // console.log(i)
  //     passageRange[1] = i;
  //     break;
  //   }
  // }

  // res.json(excerpt.slice(passageRange[0], passageRange[1]).trim());

  const title = texts.titles[Math.floor(Math.random() * texts.titles.length)];
  // console.log(title);
  // console.log(texts.getExcerpt(texts.getText(title), 1000));
  const excerpt = texts.getExcerpt(texts.getText(title), 1000);
  const meta = texts.getTitleAuthor(title);
  res.json({ excerpt, meta });
});

module.exports = router;
