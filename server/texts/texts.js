"use strict";

var fs = require("fs");

function getText(title) {
  console.log(`${__dirname}/${title}.txt`);
  return fs.readFileSync(`${__dirname}/${title}.txt`).toString();
}

function getTitleAuthor(title) {
  return {
    texts: {
      "austen-emma": {
        title: "Emma",
        author: "Jane Austen",
      },
      "austen-persuasion": {
        title: "Persuasion",
        author: "Jane Austen",
      },
      "austen-sense": {
        title: "Sense and Sensibility",
        author: "Jane Austen",
      },
      "burgess-busterbrown": {
        title: "The Adventures of Buster Bear",
        author: "Thornton W. Burgess",
      },
      "carroll-alice": {
        title: "Alice's Adventures in Wonderland",
        author: "Lewis Caroll",
      },
      "chesterton-ball": {
        title: "The Ball and the Cross",
        author: "G. K. Chesterton",
      },
      "chesterton-brown": {
        title: "The Wisdom of Father Brown",
        author: "G. K. Chesterton",
      },
      "chesterton-thursday": {
        title: "The Man who was Thursday: A Nightmare",
        author: "G. K. Chesterton",
      },
      "edgeworth-parents": {
        title: "The Parent's Assistant",
        author: "Maria Edgeworth",
      },
      "melville-moby_dick": {
        title: "Moby Dick",
        author: "Herman Melville",
      },
    },
  }.texts[title];
}

function getExcerpt(excerpt, passageLength) {
  let passageStart = Math.floor(
    Math.random() * (excerpt.length - passageLength)
  );
  let passageRange = [passageStart, passageStart + passageLength];

  for (let i = passageRange[0]; i > 0; i--) {
    if (excerpt[i - 1] == ".") {
      passageRange[0] = i;
      break;
    }
  }
  for (let i = passageRange[0]; i < passageRange[1]; i++) {
    if (/[a-zA-Z0-9]/.test(excerpt[i])) {
      passageRange[0] = i;
      break;
    }
  }

  for (let i = passageRange[1]; i < excerpt.length; i++) {
    if (excerpt[i + 1] == ".") {
      passageRange[1] = i;
      break;
    }
  }
  return excerpt.slice(passageRange[0], passageRange[1]).trim();
}

module.exports = {
  getText,
  getExcerpt,
  getTitleAuthor,
  titles: [
    "austen-emma",
    "austen-persuasion",
    "austen-sense",
    "burgess-busterbrown",
    "carroll-alice",
    "chesterton-ball",
    "chesterton-brown",
    "chesterton-thursday",
    "edgeworth-parents",
    "melville-moby_dick",
  ],
};
