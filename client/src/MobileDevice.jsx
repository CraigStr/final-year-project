import { Typography } from '@material-ui/core';
import React from 'react';

/**
 * Restricts user access on mobile devices.
 * Ensures collector can only be used with a laptop
 * (Not Foolproof, but POC)
 *
 * @return {Object} Component
 */
function MobileDevice() {
  return (
    <>
      <Typography>
        Sorry! But this experiment is based on keyboards, so sadly mobile
        devices cant take part! Please do come back if you get to a laptop or
        desptop PC!
      </Typography>
    </>
  );
}

export default MobileDevice;
