import { useEffect, useState, useRef, React } from 'react';
import { DataFrame } from 'pandas-js';
import {
  AppBar,
  CssBaseline,
  FormControlLabel,
  makeStyles,
  Paper,
  Snackbar,
  Switch,
  TextareaAutosize,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import axios from 'axios';
import { useAuth0 } from '@auth0/auth0-react';
import LogoutButton from './Components/Auth/LogoutButton';
import SubmitDialog from './Components/SubmitDialog';
/**
 * Defines CSS styles to use in component
 * @param {theme} Material-UI Theme
 */
const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(4),
    '& p': {
      padding: theme.spacing(2),
    },
  },
  AppBar: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  paper: {
    width: '100%',
    color: 'contrastText',
  },
  TA: {
    width: '100%',
  },
}));
/**
 * Component for keystroke collector page.
 * - Fetches book extract from backend
 * - Collects keystrokes into Pandas-js DataFrame
 * - Posts DataFrame to backend
 *
 * @return {*} Component
 */
function Collector() {
  const classes = useStyles();

  const TextAreaRef = useRef();

  const [DF, setDF] = useState(new DataFrame());
  const [excerpt, setExcerpt] = useState('');
  const [excerptHL, setExcerptHL] = useState('');
  const [dialogOpen, setDialogOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState('');
  const [excerptMeta, setExcerptMeta] = useState({});
  const [error, setError] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [toastOpen, setToastOpen] = useState(false);

  const [checked, setChecked] = useState({
    showHighlight: true,
    wrapText: true,
    showKD: true,
  });

  const serverUrl = process.env.REACT_APP_SERVER_URL;

  const { getAccessTokenSilently, user } = useAuth0();

  /**
   * Handles <SubmitDialog> after keystroke samples.
   * - If successful submission
   *    - Shows <SubmitDialog> with Thankful message
   *    - Clears DF so user can begin again
   * - Else
   *    - Shows <SubmitDialog> with Error message
   *
   * @param {Boolean} open
   */
  const handleSubmitDialog = (open) => {
    setDialogOpen(open);
    console.log(dialogOpen, error);
    if (!error) {
      // Clear DF
      setDF(new DataFrame());
      setExcerpt('');
      setExcerptHL('');
      setExcerptMeta({});
      TextAreaRef.current.value = '';
      // eslint-disable-next-line no-use-before-define
      getExcerpts();
    }
  };

  /**
   * Handles submission of keystroke samples
   * - Posts keystroke DataFrame to backend
   *
   * Calls handleSubmit Dialog to display success/error message
   *
   */
  const postKDs = async () => {
    try {
      if (DF.length < 100) {
        setToastOpen(true);
        console.log('Empty DF');
        return;
      }
      // Backend Auth
      const token = await getAccessTokenSilently();
      const response = await axios.post(
        `${serverUrl}`,
        {
          KD: DF.to_json(),
          email: user.email,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      setError(false);
      // Show Success Dialog
      setDialogContent(response.data.message);
      handleSubmitDialog(true);
    } catch (err) {
      // Show Error Dialog
      setError({ type: 'Network', message: err });
      setDialogContent(err.message);
      console.log(err);
      handleSubmitDialog(true);
    }
  };

  /**
   * Fetches book excerpt from backend.
   *
   */
  const getExcerpts = async () => {
    try {
      // Backend Auth
      const token = await getAccessTokenSilently();
      const response = await fetch(`${serverUrl}/excerpt`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const responseData = await response.json();
      setExcerpt(responseData.excerpt);
      setExcerptMeta(responseData.meta);
    } catch (err) {
      // Show Error Dialog
      setExcerpt(err.message);
      setDialogContent(err.message);
      setError({ type: 'Excerpt', message: err });
      handleSubmitDialog(true);
    }
  };
  // Only get Excerpts on initial load, not on re-renders
  useEffect(() => {
    getExcerpts();
  }, []);

  /**
   * Handles text highlighting on user type
   *
   * @return {Boolean} Is highlight enabled
   */
  const handleHighlight = () => {
    // Get difference in length of highlight and current typed text length
    const excHLLen = excerptHL.length;
    const TAlength = TextAreaRef.current.value.length;
    const lenDiff = TAlength - excHLLen;

    // Clear highlighting if disabled
    if (!checked.showHighlight) {
      if (excHLLen > 0) {
        setExcerpt(excerptHL + excerpt);
        setExcerptHL('');
      }
      return false;
    }

    // Change highlighed text length
    if (lenDiff !== 0) {
      if (lenDiff > 0) {
        setExcerptHL(excerptHL + excerpt.slice(0, lenDiff));
        setExcerpt(excerpt.slice(lenDiff));
      } else {
        setExcerpt(excerptHL.slice(excHLLen + lenDiff) + excerpt);
        setExcerptHL(excerptHL.slice(0, excHLLen + lenDiff));
      }
    }
    return true;
  };

  /**
   * Handles Key Presses in TextArea
   * Adds:
   *  - Action: ['KeyDown' | 'KeyUp']
   *  - Key name
   *  - Timestamp
   *  - Key Code
   *  - ASCII Code
   * to DataFrame
   * 
   * Calls for updated highlighting
   *
   * @param {Object} Keystroke Event
   */
  const handleKeyPress = (evt) => {
    if (evt.repeat) {
      if (evt && evt.preventDefault) evt.preventDefault();
      return false;
    }
    setDF(
      DF.append(
        new DataFrame([
          {
            EvtType: evt.type,
            Time: evt.timeStamp,
            Code: evt.code,
            Key: evt.key,
            // Shift: evt.shiftKey,
            // Ctrl: evt.ctrlKey,
            // Alt: evt.altKey,
            // Meta: evt.metaKey,
            KeyCode: evt.keyCode,
          },
        ]),
        true,
      ),
    );
    handleHighlight(evt.code, evt.type);
    return true;
  };

  /**
   * Updates state of corresponding checkbox
   *
   * @param {*} evt: CheckBox update
   */
  const handleCheckBox = (evt) => {
    setChecked({ ...checked, [evt.target.name]: evt.target.checked });
  };

  return (
    <CssBaseline>
      <AppBar position="static" className={classes.AppBar}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Keystroke Dynamics -{' '}
            <span style={{ fontWeight: 900 }}>Collector</span>
          </Typography>
          <FormControlLabel
            control={
              <Switch
                name="showHighlight"
                checked={checked.showHighlight}
                onChange={handleCheckBox}
              />
            }
            label="Show Highlight"
          />
          <FormControlLabel
            control={
              <Switch
                name="showKD"
                checked={checked.showKD}
                onChange={handleCheckBox}
              />
            }
            label="Show Keystroke Dynamics"
          />
          <LogoutButton />
        </Toolbar>
      </AppBar>
      <div className={classes.root}>
        <Typography variant="caption">
          This extract comes from <strong>{excerptMeta.title}</strong> written
          by <strong>{excerptMeta.author}</strong>
        </Typography>
        <Paper className={classes.paper}>
          <p style={{ whiteSpace: 'pre-wrap' }}>
            <span style={{ backgroundColor: 'yellow' }}>{excerptHL}</span>
            <span>{excerpt}</span>
          </p>
        </Paper>
        <form
          id="form"
          onSubmit={(e) => {
            e.preventDefault();
          }}
        >
          {/* <p>Focus on the input field and press a key.</p> */}

          <TextareaAutosize
            className={classes.TA}
            placeholder="Start typing here"
            id="kinput"
            ref={TextAreaRef}
            rowsMin={3}
            onKeyDown={(evt) => handleKeyPress(evt)}
            onKeyUp={(evt) => handleKeyPress(evt)}
          />
          {checked.showKD ? (
            <TextareaAutosize
              className={classes.TA}
              id="result"
              readOnly
              rowsMin={10}
              rowsMax={10}
              value={DF}
            />
          ) : (
            ''
          )}
        </form>
        <button type="button" onClick={postKDs}>
          SEND
        </button>
      </div>
      <SubmitDialog
        open={dialogOpen}
        onClose={handleSubmitDialog}
        selectedValue={dialogContent}
        error={error}
      />
      <Snackbar
        open={toastOpen}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        message={
          <Alert onClose={() => setToastOpen(false)} severity="warning">
            Your sample is too short! Please type more of the passage shown.
          </Alert>
        }
        // onClose={() => {
        //   setToastOpen(false);
        // }}
      />
    </CssBaseline>
  );
}

export default Collector;
