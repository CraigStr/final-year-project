import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';
import Collector from './Collector';
import LandingPage from './LandingPage';
import PrivateRoute from './Components/Auth/PrivateRoute';
import MobileDevice from './MobileDevice';
/**
 * Application Root Component
 * Initialises React Router
 * Prevents users on mobile devices accessing.
 *
 * @return {*} Component
 */
function App() {
  const { isLoading } = useAuth0();

  const screenSize = [window.screen.width, window.screen.height];

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      {screenSize[0] >= 1080 && screenSize[1] >= 700 ? (
        <Router>
          <Switch>
            <Route exact path="/" component={LandingPage} />
            <PrivateRoute path="/collector" component={Collector} />
          </Switch>
        </Router>
      ) : (
        <MobileDevice />
      )}
    </>
  );
}

export default App;
