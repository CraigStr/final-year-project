import { useAuth0 } from '@auth0/auth0-react';
import {
  AppBar,
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CssBaseline,
  Grid,
  makeStyles,
  Paper,
  Toolbar,
  Typography,
} from '@material-ui/core';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import AuthenticationButton from './Components/Auth/AuthenticationButton';
import ImageDialog from './Components/ImageDialog';

/**
 * Controls CSS of component elements
 */
const useStyles = makeStyles(() => ({
  AppBar: {},
  title: {
    flexGrow: 1,
  },
  indent: {
    marginLeft: '1rem',
  },
  header: {
    marginTop: '1rem',
    marginBottom: '0.5rem',
  },
}));

function LandingPage() {
  const classes = useStyles();
  const { isAuthenticated } = useAuth0();

  const [dialogState, setDialogState] = useState(false);

  return (
    <CssBaseline>
      <AppBar position="static" className={classes.AppBar}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Keystroke Dynamics
          </Typography>
          <Typography variant="h6" className={classes.title}>
            Craig Stratford - Final Year Project Maynooth University - CSSE
          </Typography>
          {isAuthenticated && (
            <Button
              component={Link}
              to="/collector"
              color="inherit"
              type="button"
              size="large"
            >
              <Typography>Collector</Typography>
            </Button>
          )}
          <AuthenticationButton size="large" />
        </Toolbar>
      </AppBar>
      <div style={{ width: '100%', padding: '1rem' }}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} m={6} lg={6} xl={6}>
            <Paper elevation={8} style={{ padding: '2rem' }}>
              <Typography variant="h4" style={{ textAlign: 'center' }}>
                &quot;Intruder Fingerprinting and Comparison with Other
                Intrusions by use of Free Text Keystroke Dynamics&quot;
              </Typography>
              <Typography
                className={classes.indent}
                style={{ marginTop: '2rem' }}
              >
                My name is <strong>Craig Stratford</strong>, a final year
                Computer Science and Software Engineering student in Maynooth
                University.
              </Typography>
              <Typography variant="h6" className={classes.header}>
                My project
              </Typography>
              <Typography className={classes.indent}>
                The Final Year Project I have chosen for this course is
                &quot;Intruder Fingerprinting and Comparison with Other
                Intrusions by use of Free Text Keystroke Dynamics&quot;.
              </Typography>
              <br />
              <Typography className={classes.indent}>
                Simply put, I plan on creating a system that will detect when an
                intruder is accessing a system, purely based on the rhythms and
                intricacies of how they type on a keyboard. These
                intruders&apos; typing patterns will then be cross-referenced
                with all previously discovered intruders, to establish a link
                between attempted intrusions.
              </Typography>
              <Typography variant="h6" className={classes.header}>
                Why?
              </Typography>
              <Typography className={classes.indent}>
                This would be a great addition for computer forensics. Forensics
                experts would be able to match an intruder to serveral different
                attacks with nothing more than their keyboard presses, allowing
                authorities to gather evidence against the individual.
              </Typography>
              <Typography variant="h6" className={classes.header}>
                What I need from you!
              </Typography>
              <Typography className={classes.indent}>
                In order to compare the keystroke dynamics of two people, I
                would first need some samples of keystroke data. This is,
                hopefully, where you come in.
              </Typography>
              <br />
              <Typography className={classes.indent}>
                In order to allow my system to properly identify each person
                correctly, each person will need to submit multiple times. If
                you would submit around 8-10 samples, that should give a good
                estimate of your typing behaviour to my system.
                <br />
                If you feel like submitting more than 10, that would be
                absolutely fantastic. If you don&apos;t feel like submitting as
                many, please submit as many as you can. As the more samples I
                have, the more accurate my results will be.
              </Typography>
              <Typography variant="h6" className={classes.header}>
                Any Issues?
              </Typography>
              <Typography className={classes.indent}>
                If you find an issue with the site, or simply wish to know more
                about this project, send me an email at{' '}
                <a href="mailto:fyp.craig.stratford@gmail.com">
                  fyp.craig.stratford@gmail.com
                </a>
                .
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} m={6} lg={6} xl={6}>
            <Paper elevation={8} style={{ padding: '2rem' }}>
              <Card
                elevation={8}
                style={{
                  margin: '0 0 1rem 1rem',
                  float: 'right',
                  width: '40%',
                }}
              >
                <CardActionArea
                  onClick={() => {
                    setDialogState(true);
                  }}
                >
                  <CardMedia component="img" image="/images/Collector.png" />
                </CardActionArea>
                <CardContent>
                  <Typography>
                    Here&apos;s what you&apos;ll see in the collector - Click to
                    enlarge
                  </Typography>
                </CardContent>
              </Card>
              <Typography variant="h4">How to submit a sample?</Typography>
              <ol>
                <li>
                  <Typography
                    variant="h5"
                    className={`${classes.indent} ${classes.header}`}
                  >
                    Log in
                  </Typography>
                  <Typography align="justify" className={classes.indent}>
                    Use the &quot;LOG IN&quot; button in the banner at the top
                    of the page to log in.
                    <br />
                    In order to keep track of whose keystroke dyanmics belong to
                    who, you will need to create an account for this page. No
                    personal data will ever be released, and I personally can
                    see no details except your email address, username and of
                    course the keystroke dynamics you submit. Feel free to
                    contact me at any time if this is of any concern.
                  </Typography>
                </li>
                <li>
                  <Typography
                    variant="h5"
                    className={`${classes.indent} ${classes.header}`}
                  >
                    Type what you see
                  </Typography>
                  <Typography className={classes.indent}>
                    You will be presented with a passage of text that you can
                    type into the textarea below it. As you type, the portion of
                    the text that you have typed will become highlighted, so you
                    can clearly see where you are in the passage.
                  </Typography>
                </li>
                <ul>
                  <li>
                    <Typography
                      variant="h6"
                      className={`${classes.indent} ${classes.header}`}
                    >
                      Dont worry about mistakes
                    </Typography>
                    <Typography className={classes.indent}>
                      If you make a mistake and notice it right away, go and
                      correct the error, but if you happen to see an error you
                      made a few lines back, no worries! You can change it or
                      leave it be. That&apos;s up to you.
                    </Typography>
                  </li>
                  <li>
                    <Typography
                      variant="h6"
                      className={`${classes.indent} ${classes.header}`}
                    >
                      Settings
                    </Typography>
                    <Typography className={classes.indent}>
                      Your Keystroke Dynamics will also be shown underneath the
                      textarea. This can also be turned off using the &quot;Show
                      Keystroke Dynamics&quot; slider in the header bar.
                    </Typography>
                    <Typography
                      className={`${classes.indent} ${classes.header}`}
                    >
                      You can also turn on and off the text highlighting,
                      mentioned in (2.) with the &quot;Show Highlight&quot;
                      slider in the header bar.
                    </Typography>
                  </li>
                </ul>
                <li>
                  <Typography
                    variant="h6"
                    className={`${classes.indent} ${classes.header}`}
                  >
                    Submit!
                  </Typography>
                  <Typography className={classes.indent}>
                    When you are finished the passage, you can simply press
                    &quot;Send&quot; to submit your Keystroke Dyanamics.
                  </Typography>
                  <br />
                  <Typography className={classes.indent}>
                    Please do remember that the more submissions you can give,
                    the better and more accurate the results of my project will
                    be. Even submitting two samples a day for 5 days will give
                    me a perfect amount of samples.
                  </Typography>
                </li>
              </ol>
              <Typography variant="h4" style={{ textAlign: 'right' }}>
                Ready to start?&nbsp;
                <span>
                  {isAuthenticated ? (
                    <Button
                      size="large"
                      component={Link}
                      to="/collector"
                      color="primary"
                      variant="contained"
                      type="button"
                    >
                      <Typography>Go!</Typography>
                    </Button>
                  ) : (
                    <AuthenticationButton
                      size="large"
                      color="primary"
                      variant="contained"
                    />
                  )}
                </span>
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>
      <ImageDialog
        open={dialogState}
        onClose={() => setDialogState(false)}
        src="/images/Collector.png"
      />
    </CssBaseline>
  );
}

export default LandingPage;
