import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogContent,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';

/**
 * Dialog containing Image
 * Responsive for mobile devices
 *
 * @param {*} { onClose, open, src }
 * @return {*} Component
 */
function ImageDialog({ onClose, open, src }) {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Dialog
      style={{ maxWidth: '100%' }}
      onClose={() => onClose()}
      open={open}
      fullScreen={fullScreen}
      maxWidth="lg"
    >
      <DialogContent>
        <img src={src} alt="Collector example" />
      </DialogContent>
    </Dialog>
  );
}

ImageDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired,
};

export default ImageDialog;
