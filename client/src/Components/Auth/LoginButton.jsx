import React from 'react';
import PropTypes from 'prop-types';
import { useAuth0 } from '@auth0/auth0-react';
import { Button } from '@material-ui/core';
/**
 * Material-UI button.
 * Brings user to Auth0 login page.
 * Specifies Return URI
 *
 * @param {*} props
 * @return {*}
 */
const LoginButton = (props) => {
  const { variant, color, size } = props;
  const { loginWithRedirect } = useAuth0();
  return (
    <Button
      color={color || 'inherit'}
      size={size || 'inherit'}
      type="button"
      variant={variant || 'inherit'}
      onClick={() =>
        loginWithRedirect({ redirect_uri: 'http://localhost:3000/' })
      }
    >
      Log In
    </Button>
  );
};

LoginButton.propTypes = {
  /**
   * Button Variant
   */
  variant: PropTypes.string,
  /**
   * Button Color
   */
  color: PropTypes.string,
  /**
   * Button Size
   */
  size: PropTypes.string,
};

LoginButton.defaultProps = {
  variant: 'inherit',
  color: 'inherit',
  size: 'inherit',
};

export default LoginButton;
