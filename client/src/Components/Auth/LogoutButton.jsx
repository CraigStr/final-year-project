import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { Button, Typography } from '@material-ui/core';
/**
 * Material-UI Button
 * Logs user out on click.
 * Returns user to Landing Page
 *
 * @return {*} Component
 */
const LogoutButton = () => {
  const { logout } = useAuth0();
  return (
    <Button
      color="inherit"
      type="button"
      onClick={() =>
        logout({
          returnTo: window.location.origin,
        })
      }
    >
      <Typography>Log Out</Typography>
    </Button>
  );
};

export default LogoutButton;
