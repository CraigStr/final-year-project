import React from 'react';

import { useAuth0 } from '@auth0/auth0-react';
import PropTypes from 'prop-types';
import LoginButton from './LoginButton';
import LogoutButton from './LogoutButton';

/**
 * Material-UI Button displaying a <LoginButton> component when not authenticated,
 * and a <LogoutButton> when authenticated
 *
 * @param {*} props
 * @return {*} Component
 */
const AuthenticationButton = (props) => {
  const { variant, color, size } = props;
  const { isAuthenticated } = useAuth0();

  return isAuthenticated ? (
    <LogoutButton />
  ) : (
    <LoginButton size={size} color={color} variant={variant} />
  );
};

AuthenticationButton.propTypes = {
  /**
   * Button Variant
   */
  variant: PropTypes.string,
  /**
   * Button Color
   */
  color: PropTypes.string,
  /**
   * Button Size
   */
  size: PropTypes.string,
};

AuthenticationButton.defaultProps = {
  variant: 'inherit',
  color: 'inherit',
  size: 'inherit',
};

export default AuthenticationButton;
