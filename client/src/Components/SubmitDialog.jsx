import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
} from '@material-ui/core';

/**
 * Dialog showing:
 * - Response thanking user for a successful submission
 * - Message informing of NetworkError
 * - Message informing that submission is too short.
 *
 * @param {*} { onClose, selectedValue, open, error }
 * @return {*} Component
 */
function SubmitDialog({ onClose, selectedValue, open, error }) {
  return (
    <Dialog
      onClose={() => onClose(false)}
      aria-labelledby="simple-dialog-title"
      open={open}
      disableBackdropClick={error.type === 'Excerpt'}
      disableEscapeKeyDown={error.type === 'Excerpt'}
    >
      {error ? (
        <>
          <DialogTitle id="simple-dialog-title">
            Something went Wrong! - {selectedValue}
          </DialogTitle>
          <DialogContent>
            <br />
            <Typography>
              Please{' '}
              {error.type === 'Excerpt' && (
                <span>
                  <Button
                    variant="contained"
                    onClick={() => {
                      window.location.reload(true);
                      return false;
                    }}
                  >
                    reload
                  </Button>{' '}
                  and
                </span>
              )}{' '}
              try again
            </Typography>
            {error.type === 'Network' && (
              <Button
                variant="contained"
                color="primary"
                style={{ float: 'right' }}
                onClick={() => onClose()}
              >
                Close
              </Button>
            )}
          </DialogContent>
        </>
      ) : (
        <>
          <DialogTitle id="simple-dialog-title">{selectedValue}</DialogTitle>
          <DialogContent>
            <Typography>Thank you for submitting your keystrokes!</Typography>
            <Typography>
              The more times you can do this the more accurate my results will
              become.
            </Typography>
            <Typography>
              So if you havent submitted several of these, please consider
              completing this again, as it will really help me. You don&apos;t
              need to do it just this second, but please do come back and
              complete this again when you have the time!
            </Typography>
            <Typography>
              If you have the time now, press the button below to start anew!
            </Typography>
            <Typography>Again, Many Thanks!</Typography>
            <Button onClick={() => onClose(false)} color="primary">
              Complete Again?
            </Button>
          </DialogContent>
        </>
      )}
    </Dialog>
  );
}

SubmitDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  selectedValue: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  error: PropTypes.exact({
    type: PropTypes.string.isRequired,
  }).isRequired,
};

export default SubmitDialog;
